<Query Kind="Program" />

void Main() 
{ 
    const string fileName = @"c:\temp\scores.csv"; 
    String[] lines = File.ReadAllLines(fileName); 
    //lines.Dump(); 
     
   // if no data 
    if (lines.Count() < 2 ) { 
                 "No data".Dump(); 
                     return; 
    } 
     
     //convert data from string to collection 
    var students =  lines 
                        .Skip(1) 
                        .Select(s => s.Split(',')) 
                        .Select(a => new { 
                                            ID = a[0],  
                                            Name = a[1],  
                                            Subject = a[2],  
                                            Scores = Convert.ToInt32(a[3]) 
                        })                 
                        .ToList(); 
         
         
    int subjectCount = students.GroupBy(s => s.Subject).Count(); 
         
                 
    var studentsWithHighAvg = students 
           .GroupBy(r =>  r.Name) 
           .Select( s => new { 
                   Name = s.Key,  
                   // if subject has several scores take max scores 
                   Score_Average = ((decimal) s
				                            .GroupBy(x => x.Subject)
				                            .Select(x => x.Max(xx => xx.Scores))
											.Sum()) / subjectCount })	                                                     
           .GroupBy(r => r.Score_Average) 
           .OrderByDescending(r => r.Key) 
           //select all students with the same highest average by name 
           .FirstOrDefault() 
           .OrderBy(r => r.Name); 
     

	
	//Result			
	"The Students with the highest average:".Dump();					   
	studentsWithHighAvg.Dump();	

    //Data
	"All Students:".Dump();					 
	students.OrderBy(s =>s.Name).ThenBy(s => s.Subject).Dump();
	
}