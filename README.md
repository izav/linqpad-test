# [Test](https://stackoverflow.com/jobs/62663/can-you-answer-this-c-sharp-challenge-looking-for-a-courtalert-a-legal) #

Download [this CSV](http://www.courtalert.com/jobs/Interview.zip) with a row id, student name, subject and score.


![ST1.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/ST1.jpg)

A student can have not all subjects   
A student can have several scores for the same subject

```
1,Jim,Math,55
18,Jim,English, 89  
35,Jim,Spanish,71   
45,Jim,Spanish,93 

```


```
4,Kevin,Math,21
12,Kevin,Science,87
29,Kevin,English,92
38,Kevin,Spanish,95
46,Kevin,Spanish,69

```


Calculate the average for each student and display the studentís name with the highest average.
Write the code in Linqpad (http://www.linqpad.net/).
Included is a .linq file you can use as a starting point.


```
#!c#

void Main()

{ 
 
	const string fileName = @"c:\temp\scores.csv";

	String[] lines = File.ReadAllLines(fileName);

	//output the lines

	lines.Dump();
	
	//todo calculate the average for each student and display the studentís name with the highest average.

}
 
```






# Result #



![TestResult[1].jpg](https://bitbucket.org/repo/ba6on7o/images/544622266-TestResult%5B1%5D.jpg)